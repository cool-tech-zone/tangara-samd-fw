<!--
Copyright 2023 jacqueline <me@jacqueline.id.au>

SPDX-License-Identifier: CC0-1.0
-->

# Building

You will need to have `arm-none-eabi-gcc` available on your `$PATH`, and check
out the library submodules with:

```
git submodule update --init --recursive
```

Once you've done this, the Makefile in the root of this repo can be used to
build the firmware.

```
make
```

You can also conveniently flash the built firmware to a mounted UF2 partition
by using `make flash`.

The CFLAGS flag can be set to pass additional build flags to GCC. For example,
if you have a mainboard before r9 and therefore require a build that uses the
older pinout, you can run `make CFLAGS="-DLEGACY_PINOUT"`.

# Development

Suggested `.clangd` settings:

```
CompileFlags:
  Add: [
    -ferror-limit=0,
  ]
  Remove: [
    -mcpu=,
  ]
  Compiler: /path/to/your/clang/if/needed/here/bin/clang
```
