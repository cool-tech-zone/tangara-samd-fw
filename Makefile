# Copyright 2024 jacqueline <me@jacqueline.id.au>
#
# SPDX-License-Identifier: GPL-3.0-only

.PHONY : clean build flash

build:
	@mkdir -p build
	@cd build; cmake .. -DEXTRA_C_FLAGS="${CFLAGS}"; make

flash: build
	./tools/uf2/utils/uf2conv.py build/tangara.uf2 -D

clean:
	@rm -rf build/
