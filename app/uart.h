/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

void uart_init(void);

bool uart_getc(char* c);
void uart_putc(char c);
void uart_puts(char* s);
int uart_printf(const char* format, ...);

/*
 * Changes the baud rate used for outputing over UART. The maximum value, given
 * a 48MHz clock, is 1,500,000. Values higher than this will be ignored.
 */
void uart_configure(uint32_t baud_rate);
