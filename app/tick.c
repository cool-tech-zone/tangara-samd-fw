/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "tick.h"

#include <atmel_start.h>
#include <stdint.h>
#include "samd21e18a.h"
#include "system_samd21.h"

static volatile uint32_t sTicks;

void SysTick_Handler() {
  if (sTicks == UINT32_MAX) {
    sTicks = 0;
  } else {
    sTicks++;
  }
}

void tick_init() {
  SysTick->CTRL = 0;
  // Always tick, even if other interrupts are long-running.
  NVIC_SetPriority(SysTick_IRQn, (1UL << __NVIC_PRIO_BITS) - 1UL);
  // Trigger the interrupt once per millisecond.
  SysTick->LOAD = (48000000 / 1000) - 1;
  SysTick->VAL = 0;
  SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk |
                  // Enable interrupts.
                  SysTick_CTRL_TICKINT_Msk |
                  // Enable SysTick
                  SysTick_CTRL_ENABLE_Msk;
}

uint32_t tick_millis() {
  return sTicks;
}
