/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "atmel_start.h"
#include "class/cdc/cdc_device.h"
#include "component/dmac.h"
#include "component/gclk.h"
#include "component/pm.h"
#include "component/port.h"
#include "component/sercom.h"
#include "component/sysctrl.h"
#include "component/wdt.h"
#include "device/usbd.h"
#include "gpio.h"
#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "pio/samd21e18a.h"
#include "samd21e18a.h"
#include "sd.h"
#include "tick.h"
#include "tusb.h"

#include "cdc.h"
#include "i2c.h"
#include "msc.h"
#include "spi.h"
#include "uart.h"

static void brownout_det_init(void);
static void cpu_clock_init(void);
static void wdt_init(void);
static void wdt_feed(void);
static void usb_init(void);

void USB_Handler(void) {
  tud_int_handler(0);
}

int main(void) {
  // Initial hardware setup.
  gpio_early_init();
  brownout_det_init();
  cpu_clock_init();
  wdt_init();
  tick_init();

  // Peripherals
  gpio_init();
  uart_init();
  i2c_init();
  spi_init();

  // Higher-level modules (USB stack and SD card dribe)
  usb_init();
  tusb_init();
  sd_init();

  // Get initial GPIO readings.
  gpio_task();

  // Let the ESP32 start booting.
  // FIXME: If there's no battery and our USB current limit is only 100mA, then
  // we shouldn't try turning on the ESP32.
  gpio_set_pin_level(PIN_ESP_EN, true);

  bool usb_attached = false;
  bool usb_busy = false;
  bool cdc_dirty = false;
  while (1) {
    bool active = false;
    tud_task();
    sd_task();

    char c;
    if (uart_getc(&c)) {
      cdc_dirty = true;
      active = true;
      cdc_putc(c);
    } else if (cdc_dirty) {
      // Ensure that we don't end up with leftover bytes stuck in the buffer,
      // since tinyusb doesn't flush regularly when left alone.
      cdc_flush();
      cdc_dirty = false;
    }

    if (tud_cdc_available()) {
      uint8_t buf[64];
      uint32_t count = tud_cdc_read(buf, sizeof(buf));
      for (uint32_t i = 0; i < count; i++) {
        uart_putc(buf[i]);
      }
      active = true;
    }

    if (active) {
      // The UART <-> USB CDC bridge needs to support very high baudrates for
      // flashing to be fast. Prioritise it over the other system functions.
      continue;
    }

    gpio_task();

    bool new_usb_busy = (tick_millis() - msc_last_io_ticks()) < 1000;
    bool new_usb_attached = tud_ready();
    if (new_usb_attached != usb_attached || new_usb_busy != usb_busy) {
      usb_attached = new_usb_attached;
      usb_busy = new_usb_busy;
      i2c_register_set(USB_STATUS, (usb_busy << 1) | usb_attached);
    }

    uint8_t power_ctl = i2c_register_get(POWER_CONTROL);
    if (power_ctl & 0b1) {
      // Turn off the ESP32
      gpio_set_pin_level(PIN_ESP_EN, false);
      // Turn off the system.
      gpio_set_pin_level(PIN_SYS_PWR_EN, false);

      // FIXME: If the lock switch is still unlocked, then we'll keep running!
      // Do we want to do anything in this situation? Reboot the ESP32 maybe?
      continue;
    }

    uint8_t usb_ctl = i2c_register_get(USB_CONTROL);
    if (usb_ctl & 0b100) {
      // UF2 mode requested. See inc/uf.h in the bootloader repo; on reset, the
      // bootloader checks this address to determine whether it should boot the
      // application or stay in the bootloader.
      volatile uint32_t* sMagicPtr =
          (volatile uint32_t*)(HMCRAMC0_ADDR + HMCRAMC0_SIZE - 4);
      *sMagicPtr = 0xf01669ef;
      NVIC_SystemReset();
    }

    wdt_feed();
  }
}

/*
 * Waits for input voltage to stabilise, then sets up the brownout detector to
 * reset if input voltage falls too low.
 */
static void brownout_det_init() {
  // Disable the brownout detector whilst we're configuring it!!
  SYSCTRL->BOD33.bit.ENABLE = 0;
  while (!SYSCTRL->PCLKSR.bit.B33SRDY)
    ;

  // Configure the brown-out detector so that the program can use it to watch
  // the power supply voltage.
  SYSCTRL->BOD33.reg = (
      // This sets the minimum voltage level to 2.8v - 2.9v.
      // See datasheet table 37-21.
      SYSCTRL_BOD33_LEVEL(39) |
      // Since the program is waiting for the voltage to rise, don't reset the
      // microcontroller if the voltage is too low.
      SYSCTRL_BOD33_ACTION_NONE |
      // Enable hysteresis to better deal with noisy power supplies and voltage
      // transients.
      SYSCTRL_BOD33_HYST);

  // Enable the brown-out detector and then wait for the voltage level to
  // settle.
  SYSCTRL->BOD33.bit.ENABLE = 1;
  while (!SYSCTRL->PCLKSR.bit.BOD33RDY)
    ;

  // BOD33DET is set when the voltage is too low, so wait for it to be cleared.
  while (SYSCTRL->PCLKSR.bit.BOD33DET)
    ;

  // Let the brown-out detector automatically reset the microcontroller if the
  // voltage drops too low.
  SYSCTRL->BOD33.bit.ENABLE = 0;
  while (!SYSCTRL->PCLKSR.bit.B33SRDY)
    ;
  SYSCTRL->BOD33.reg |= SYSCTRL_BOD33_ACTION_RESET;

  // We're finished! Turn it back on.
  SYSCTRL->BOD33.bit.ENABLE = 1;
}

/*
 * Sets up the DFLL to output 48MHz, based on either USB SOF clock recovery or
 * the low-power 32.768kHz osclillator. Then sets up GCLK0 to use DFLL output
 * and based the CPU clock on that.
 */
static void cpu_clock_init() {
  // Set the correct number of wait states for 48 MHz @ 3.3v.
  NVMCTRL->CTRLB.bit.RWS = 1;

  // This works around a quirk in the hardware (errata 1.2.1) - the DFLLCTRL
  // register must be manually reset to this value before configuration.
  while (!SYSCTRL->PCLKSR.bit.DFLLRDY)
    ;
  SYSCTRL->DFLLCTRL.reg = SYSCTRL_DFLLCTRL_ENABLE;
  while (!SYSCTRL->PCLKSR.bit.DFLLRDY)
    ;

  // Write the coarse and fine calibration from NVM.
  uint32_t coarse = ((*(uint32_t*)FUSES_DFLL48M_COARSE_CAL_ADDR) &
                     FUSES_DFLL48M_COARSE_CAL_Msk) >>
                    FUSES_DFLL48M_COARSE_CAL_Pos;
  uint32_t fine = ((*(uint32_t*)FUSES_DFLL48M_FINE_CAL_ADDR) &
                   FUSES_DFLL48M_FINE_CAL_Msk) >>
                  FUSES_DFLL48M_FINE_CAL_Pos;

  SYSCTRL->DFLLVAL.reg =
      SYSCTRL_DFLLVAL_COARSE(coarse) | SYSCTRL_DFLLVAL_FINE(fine);

  // Wait for the write to finish.
  while (!SYSCTRL->PCLKSR.bit.DFLLRDY)
    ;

  SYSCTRL->DFLLCTRL.reg |=
      // Enable USB clock recovery mode.
      SYSCTRL_DFLLCTRL_USBCRM |
      // Disable chill cycle as per datasheet to speed up locking. This is
      // specified in section 17.6.7.2.2, and chill cycles are described in
      // section 17.6.7.2.1.
      SYSCTRL_DFLLCTRL_CCDIS;

  // Configure the DFLL to multiply the 1 kHz clock to 48 MHz.
  SYSCTRL->DFLLMUL.reg =
      // This value is output frequency / reference clock frequency, so
      // 48 MHz / 1 kHz.
      SYSCTRL_DFLLMUL_MUL(48000) |
      // The coarse and fine values can be set to their minimum since coarse is
      // fixed in USB clock recovery mode and fine should lock on quickly.
      SYSCTRL_DFLLMUL_FSTEP(1) | SYSCTRL_DFLLMUL_CSTEP(1);

  // Closed loop mode.
  SYSCTRL->DFLLCTRL.bit.MODE = 1;
  // Enable the DFLL.
  SYSCTRL->DFLLCTRL.bit.ENABLE = 1;

  // Wait for the write to finish.
  while (!SYSCTRL->PCLKSR.bit.DFLLRDY) {
  };

  // Setup GCLK0 using the DFLL @ 48 MHz.
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(0) | GCLK_GENCTRL_SRC_DFLL48M |
                      GCLK_GENCTRL_IDC | GCLK_GENCTRL_GENEN;

  // Wait for the write to complete.
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // We don't need the 8MHz osc. anymore, so power it down.
  SYSCTRL->OSC8M.bit.ENABLE = 0;

  // GCLKs 3 onwards are unused. Explicitly disable them and point them at an
  // unused GCLK source to save ~2mA.
  for (size_t i = 3; i < GCLK_NUM; i++) {
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(i) | GCLK_CLKCTRL_GEN_GCLK1;
    while (GCLK->STATUS.bit.SYNCBUSY)
      ;
  }
}

static void wdt_init(void) {
  // The WDT is designed to be used with the low-power 32kHz internal
  // oscillator. Start by setting up GCLK2 to use this oscillator.

  // Divide the source by 32, to give us a 1.024kHz clock.
  GCLK->GENDIV.reg = GCLK_GENDIV_DIV(4) | GCLK_GENDIV_ID(2);
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Set up GCLK2.
  GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(2) | GCLK_GENCTRL_SRC_OSCULP32K |
                      GCLK_GENCTRL_IDC | GCLK_GENCTRL_GENEN |
                      GCLK_GENCTRL_DIVSEL;
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Connect GCLK2 to the WDT.
  GCLK->CLKCTRL.reg =
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK2 | GCLK_CLKCTRL_ID_WDT;
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Configure the WDT to trigger every 2K clock cycles. This is about two
  // seconds, give or take inaccuracies from the LP oscillator.
  WDT->CONFIG.bit.PER = WDT_CONFIG_PER_2K;
  while (WDT->STATUS.bit.SYNCBUSY)
    ;

  // Off we go!
  WDT->CTRL.bit.ENABLE = 1;
  while (WDT->STATUS.bit.SYNCBUSY)
    ;
}

static void wdt_feed(void) {
  if (!WDT->STATUS.bit.SYNCBUSY) {
    WDT->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;
  }
}

static void usb_init(void) {
  // Set our pins to the correct mux, etc.
  gpio_set_pin_function(PIN_PA24, PINMUX_PA24G_USB_DM);
  gpio_set_pin_function(PIN_PA25, PINMUX_PA25G_USB_DP);

  // USB Clock init.
  GCLK->CLKCTRL.reg =
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_USB;

  // Wait for the write to complete.
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  PM->AHBMASK.reg |= PM_AHBMASK_USB;
  PM->APBBMASK.reg |= PM_APBBMASK_USB;
}
