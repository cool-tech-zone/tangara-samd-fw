/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stddef.h>

void cdc_putc(char c);
void cdc_puts(char* c, size_t len);
void cdc_flush();
int cdc_printf(const char* format, ...);
