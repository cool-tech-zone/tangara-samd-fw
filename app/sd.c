/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "sd.h"

#include <atmel_start.h>
#include <stdint.h>

#include "cdc.h"
#include "gpio.h"
#include "i2c.h"
#include "spi.h"
#include "tick.h"
#include "uart.h"

typedef enum {
  DISABLED,
  INIT,
  INIT_FAILED,
  WAITING_FOR_INIT,
  READY,
} SdState_t;

static SdState_t sState = DISABLED;
static uint32_t sBlockCount = 0;

void sd_init() {
  sState = DISABLED;
}

void sd_begin_transaction() {
  spi_transmit(0xFF);
  gpio_set_pin_level(PIN_CS, false);
  spi_transmit(0xFF);
}

void sd_end_transaction() {
  spi_transmit(0xFF);
  gpio_set_pin_level(PIN_CS, true);
  spi_transmit(0xFF);
}

uint8_t sd_await_response() {
  uint8_t res = 0xFF;
  for (size_t i = 0; i < 8; i++) {
    res = spi_transmit(0xFF);
    if (res != 0xFF) {
      break;
    }
  }
  return res;
}

void sd_send_cmd(uint8_t cmd, uint32_t args, uint8_t crc) {
  // Each command is 6 bits, prefixed with a start bit of '1'.
  spi_transmit(cmd | (1 << 6));

  // Args are sent MSB first.
  spi_transmit((uint8_t)(args >> 24));
  spi_transmit((uint8_t)(args >> 16));
  spi_transmit((uint8_t)(args >> 8));
  spi_transmit((uint8_t)(args));

  // CRC is 7 bits long, but we tack the stop bit of '1' onto the end.
  spi_transmit(crc | 1);
}

void sd_begin() {
  // Refer to:
  //  - http://elm-chan.org/docs/mmc/mmc_e.html
  //  - http://www.rjhcoding.com/avrc-sd-interface-1.php
  // for initialising an SD card in SPI mode. The ESP32's responsibility is to
  // power on the SD card and configure the SPI mux to give us access.

  // Step 1: Prepare the card for native operating mode.
  spi_set_baud(400000);              // Start at 100-400kHz
  gpio_set_pin_level(PIN_CS, true);  // hold CS high
  // Clock out at least 74 bits.
  for (size_t i = 0; i < 10; i++) {
    spi_transmit(0xFF);
  }

  // Step 2: Send CMD0 (software reset) with CS held low.
  gpio_set_pin_level(PIN_CS, false);  // hold CS low
  spi_transmit(0xFF);       // clock out a byte so the card sees the cs change
  sd_send_cmd(0, 0, 0x94);  // send CMD0
  uint8_t res = sd_await_response();
  sd_end_transaction();
  if (res > 1) {
    // Response of '1' indicates that the card is in the idle state. Any other
    // set bits indicate various error conditions.
    sState = INIT_FAILED;
    return;
  }

  // Step 3: Send CMD8 to determine what kind of SD card we're working with.
  // Cards older than SDv2 will respond to this with a 'command not found'
  // error.
  sd_begin_transaction();
  // We send CMD8 with a VHS of 0b1 (2.7V - 3.6V), and a check pattern of
  // 0b10101010.
  sd_send_cmd(8, 0x1AA, 0x86);  // send CMD8
  res = sd_await_response();    // First byte is an R1
  if (res > 1) {
    // If this command failed, then we likely have a very old card that needs
    // to be initialised using CMD1.
    if (res & 0b100) {
      // FIXME: Handle this kind of card.
    }
    sState = INIT_FAILED;
    sd_end_transaction();
    return;
  }
  uint8_t details[4];  // The rest of the response is 4 bytes long
  for (size_t i = 0; i < 4; i++) {
    details[i] = spi_transmit(0xFF);
  }
  sd_end_transaction();

  if (details[3] != 0b10101010) {
    // The check pattern was wrong; something must be very wrong with the SD
    // card, or with the SPI bus.
    sState = INIT_FAILED;
    return;
  }
  // We don't care about the rest of the response. Our voltage range should be
  // supported by all SD cards.

  // We could now optionally send CMD58 to determine the operating voltage of
  // the card. We don't bother with this step, since our operating range of 3v
  // to 3v3 should be supported by every card.

  sState = WAITING_FOR_INIT;
}

static uint32_t sLastCheck;

void sd_wait_for_init() {
  // SD card init can take a very long time depending on the exact SD card.
  // A whole second is not unusual. Make sure we throttle polling so that we're
  // not spinlocking while we wait for the SD card to init.
  uint32_t now = tick_millis();
  if (now < sLastCheck || now - sLastCheck < 100) {
    return;
  }
  sLastCheck = now;

  uint8_t res;
  // Send CMD55, notifying the card that the next command is app-specific.
  sd_begin_transaction();
  sd_send_cmd(55, 0, 0);
  res = sd_await_response();
  sd_end_transaction();
  if (res > 1) {
    sState = INIT_FAILED;
    return;
  }

  sd_begin_transaction();
  // Set bit 30 to indicate our support for SDHC.
  sd_send_cmd(41, 1 << 30, 0);
  res = sd_await_response();
  sd_end_transaction();
  if (res > 1) {
    sState = INIT_FAILED;
    return;
  } else if (res == 1) {
    // We need to wait briefly, then send ACMD41 again.
    return;
  }
  // res == 0; init has finished!

  // Step 4: Send CMD58 (OCR). This tells us more details about the SD card.
  sd_begin_transaction();
  sd_send_cmd(58, 0, 0);  // send CMD58
  res = sd_await_response();
  if (res > 1) {
    sState = INIT_FAILED;
    sd_end_transaction();
    return;
  }
  uint8_t details[4];
  for (size_t i = 0; i < 4; i++) {
    details[i] = spi_transmit(0xFF);
  }
  sd_end_transaction();

  if (!(details[0] & (1 << 7))) {
    // Card is still booting?
    return;
  }

  // At this point, the card has now booted. Hooray!
  // Before we can expose the card over USB, we need some additional details
  // such as the maximum read speed and total card size. These are in the CSD
  // register.
  sd_begin_transaction();
  sd_send_cmd(9, 0, 0);  // Send CMD9

  // CMD9 read responses are the same as block read responses; first an R1,
  // then a delay, then a start token, then the data, then a 2-byte CRC.
  res = sd_await_response();
  if (res > 0) {
    sState = INIT_FAILED;
    sd_end_transaction();
    return;
  }

  // Wait for the start token.
  bool got_start_token = false;
  for (int i = 0; i < 16; i++) {
    if (spi_transmit(0xFF) == 0xFE) {
      got_start_token = true;
      break;
    }
  }

  if (!got_start_token) {
    sState = INIT_FAILED;
    sd_end_transaction();
    return;
  }

  // Read the CSD.
  uint8_t csd[16];
  for (size_t i = 0; i < 16; i++) {
    csd[i] = spi_transmit(0xFF);
  }

  // Discard the CRC.
  // FIXME: Implement SD CRC checks.
  spi_transmit(0xFF);
  spi_transmit(0xFF);

  sd_end_transaction();

  // The first two bits of the CSD indicate the CSD schema version.
  uint8_t csd_ver = (0b11000000 & csd[0]) >> 6;
  if (csd_ver == 0) {
    // CSD v1
    // FIXME: Implement this.
    sState = INIT_FAILED;
    return;
  } else if (csd_ver == 1) {
    // CSD v2
    uint32_t c_size = ((csd[7] & 0b00111111) << 16) | (csd[8] << 8) | csd[9];
    sBlockCount = (c_size + 1) * 1024;
  }

  // For safety and improved compatibility with tiny cards, explicitly set the
  // block size to 512 bytes.
  sd_begin_transaction();
  sd_send_cmd(16, 512, 0);
  sd_await_response();  // Don't care about the result.
  sd_end_transaction();

  // We could read TRAN_SPEED from the CSD, but in practice all SD cards should
  // support our maximum SPI speed of 12MHz.
  spi_set_baud(12000000);

  sState = READY;
}

void sd_end(void) {
  // FIXME: Work out if there's anything we can do to safely prepare the SD
  // card for poweroff. Maybe just reset to idle?
}

void sd_task() {
  bool enabled = i2c_register_get(USB_CONTROL) & 1;
  if (sState == DISABLED && enabled) {
    sState = INIT;
  } else if (sState != DISABLED && !enabled) {
    sState = DISABLED;
    sd_end();
  }

  switch (sState) {
    case DISABLED:
    case INIT_FAILED:
      return;
    case INIT:
      sd_begin();
      break;
    case WAITING_FOR_INIT:
      sd_wait_for_init();
      break;
    case READY:
      break;
  }
}

bool sd_is_ready() {
  return sState == READY;
}

uint32_t sd_block_count() {
  return sBlockCount;
}

static inline void sd_wait_for_ready() {
  while (spi_transmit(0xFF) == 0x00) {
  }
}

uint32_t sd_read(uint32_t block, uint8_t* buffer, uint32_t bufsize) {
  sd_begin_transaction();
  sd_wait_for_ready();
  sd_send_cmd(18, block, 0);
  if (sd_await_response() > 0) {
    goto error;
  }

  // Receive the block data.
  for (size_t i = 0; i < bufsize / 512; i++) {
    // Wait for a start or error token.
    uint8_t token;
    uint32_t start = tick_millis();
    while ((token = spi_transmit(0xFF)) == 0xFF) {
      if (tick_millis() - start > 250) {
        goto error;
      }
    }

    // Handle error tokens.
    if (token != 0xFE) {
      goto error;
    }

    spi_transmit_bulk(NULL, buffer + (i * 512), 512);

    // FIXME: Calculate and compare CRC instead of discarding it.
    spi_transmit(0xFF);
    spi_transmit(0xFF);
  }

  sd_send_cmd(12, 0, 0);
  spi_transmit(0xFF);  // stuff byte
  if (sd_await_response() > 0) {
    goto error;
  }

  sd_end_transaction();
  return bufsize;

error:
  sd_end_transaction();
  return -1;
}

uint32_t sd_write(uint32_t block, uint8_t* buffer, uint32_t bufsize) {
  sd_begin_transaction();
  sd_wait_for_ready();

  // On some cards, we can allegedly gain a performance improvement by
  // prefixing multi-block writes with an ACMD23 telling the card how many
  // blocks we're about to write.
  sd_send_cmd(55, 0, 0); // CMD55: Next command is application-specific
  if (sd_await_response() > 0) {
    goto error;
  }
  sd_send_cmd(23, bufsize / 512, 0); // ACMD23: SET_WR_BLOCK_ERASE_COUNT
  if (sd_await_response() > 0) {
    // FIXME: Can this fail?
    goto error;
  }

  sd_send_cmd(25, block, 0); // CMD25: WRITE_MULTIPLE_BLOCK
  if (sd_await_response() > 0) {
    goto error;
  }

  for (size_t i = 0; i < bufsize / 512; i++) {
    // Write start token.
    spi_transmit(0xFC);

    // Write the block's data.
    spi_transmit_bulk(buffer + (i * 512), NULL, 512);

    // Wait for the response token.
    uint8_t resp_token;
    while ((resp_token = spi_transmit(0xFF)) == 0xFF) {
    }

    // Check the write was accepted.
    if ((resp_token & 0b11111) != 0b00101) {
      goto error;
    }

    sd_wait_for_ready();
  }

  spi_transmit(0xFD);

  // The next step of a write is to wait for the SD card to stop signalling
  // that it's busy. Instead of doing this here, we check at the beginning of
  // each read/write. This helps speed up writes by allowing us to do USB comms
  // whilst the SD card finishes its internal write.

  sd_end_transaction();
  return bufsize;

error:
  sd_end_transaction();
  return -1;
}
