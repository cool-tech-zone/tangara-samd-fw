/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "i2c.h"

#include <atmel_start.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "component/gclk.h"
#include "component/sercom.h"
#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "pio/samd21e18a.h"
#include "samd21e18a.h"

#include "gpio.h"

#ifdef LEGACY_PINOUT

#define I2C_SERCOM SERCOM1
#define I2C_IRQ_HANDLER SERCOM1_Handler
#define I2C_IRQN SERCOM1_IRQn
#define I2C_APBCMASK PM_APBCMASK_SERCOM1
#define I2C_CLKID GCLK_CLKCTRL_ID_SERCOM1_CORE
#define I2C_SCL_PINMUX PINMUX_PA17C_SERCOM1_PAD1
#define I2C_SDA_PINMUX PINMUX_PA16C_SERCOM1_PAD0

#else

#define I2C_SERCOM SERCOM3
#define I2C_IRQ_HANDLER SERCOM3_Handler
#define I2C_IRQN SERCOM3_IRQn
#define I2C_APBCMASK PM_APBCMASK_SERCOM3
#define I2C_CLKID GCLK_CLKCTRL_ID_SERCOM3_CORE
#define I2C_SCL_PINMUX PINMUX_PA17D_SERCOM3_PAD1
#define I2C_SDA_PINMUX PINMUX_PA16D_SERCOM3_PAD0

#endif

#define I2C_NUM_REGISTERS (6)
#define I2C_NUM_RO_REGISTERS (4)

static const uint8_t kAddress = 0x45;
static const uint8_t kFirmwareMajor = 0x06;
static const uint8_t kFirmwareMinor = 0x00;

static bool sAwaitingFirstWrite = false;

static uint8_t sI2CRegisters[I2C_NUM_REGISTERS] = {
    // Register 0: firmware major version (read-only)
    kFirmwareMajor,
    // Register 1: firmware minor version (read-only)
    kFirmwareMinor,
    // Register 2: charge status (read-only)
    //  01 : usb attach state
    // 234 : charge status
    0b00000,
    // Register 3: usb status (read-only)
    // 0: samd is attached to a compatible usb host
    // 1: samd is writing to sd card
    0b00,
    // Register 4: power control
    // 0: power down esp32
    // 1: enable fast charging
    0b00,
    // Register 5: usb control
    // 0: enable usb msc
    // 1: enable usb cdc
    // 2: enable uf2 flashing
    0b000,
};

static uint8_t sActiveReg = 0;

void i2c_init() {
  // Mux the pins we're using correctly.
  gpio_set_pin_function(PIN_SCL, I2C_SCL_PINMUX);
  gpio_set_pin_function(PIN_SDA, I2C_SDA_PINMUX);

  // Connect GCLK0 to I2C_SERCOM.
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | I2C_CLKID;

  // Wait for the write to complete.
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Enable the bus clock for I2C_SERCOM.
  PM->APBCMASK.reg |= I2C_APBCMASK;

  // Reset into a consistent (and disabled) state.
  I2C_SERCOM->I2CS.CTRLA.bit.SWRST = 1;
  while (I2C_SERCOM->I2CS.SYNCBUSY.bit.SWRST)
    ;

  I2C_SERCOM->I2CS.CTRLA.reg =
      // We aren't the controller on this bus
      SERCOM_I2CS_CTRLA_MODE_I2C_SLAVE |
      // Hold SDA low for at least 300ns per pulse
      SERCOM_I2CS_CTRLA_SDAHOLD(0x2) |
      // Support high speed mode
      SERCOM_I2CS_CTRLA_SPEED(0x2) |
      // Generate address match interrupts whilst sleeping
      SERCOM_I2CS_CTRLA_RUNSTDBY;

  I2C_SERCOM->I2CS.CTRLB.reg =
      // Enable smart mode (automatically send ACK in some circumstances).
      SERCOM_I2CS_CTRLB_SMEN;

  I2C_SERCOM->I2CS.ADDR.reg =
      // Mask identical to address because we don't really want to use a mask.
      SERCOM_I2CS_ADDR_ADDRMASK(0) | SERCOM_I2CS_ADDR_ADDR(kAddress);

  // Interrupt configuration.
  I2C_SERCOM->I2CS.INTENSET.reg =
      // Enable address match interrupts.
      SERCOM_I2CS_INTENSET_AMATCH |
      // Enable data ready interrupts.
      SERCOM_I2CS_INTENSET_DRDY |
      // Enable stop condition interrupts.
      SERCOM_I2CS_INTENSET_PREC;

  I2C_SERCOM->I2CS.CTRLA.bit.ENABLE = 1;
  while (I2C_SERCOM->I2CS.SYNCBUSY.bit.ENABLE)
    ;

  NVIC_EnableIRQ(I2C_IRQN);
}

void I2C_IRQ_HANDLER() {
  // Clear the interrupt flag.
  gpio_set_pin_level(PIN_INT, true);

  // Check for a transaction start.
  if (I2C_SERCOM->I2CS.INTFLAG.bit.AMATCH) {
    // We treat the first write as an address select.
    sAwaitingFirstWrite = true;
    // Clear the flag. Smart mode is enabled, so this will respond with an ACK.
    I2C_SERCOM->I2CS.INTFLAG.reg = SERCOM_I2CS_INTFLAG_AMATCH;
  }

  // Check for either data received, or data ready to send.
  if (I2C_SERCOM->I2CS.INTFLAG.bit.DRDY) {
    if (!I2C_SERCOM->I2CS.STATUS.bit.DIR) {
      // This is a controller write / peripheral read.
      if (sAwaitingFirstWrite) {
        // This is the first write after an address match. It indicates which
        // register the next read or write applies to.
        sActiveReg = I2C_SERCOM->I2CS.DATA.reg;
        sAwaitingFirstWrite = false;
      } else {
        volatile uint8_t data = I2C_SERCOM->I2CS.DATA.reg;
        if (sActiveReg >= I2C_NUM_RO_REGISTERS) {
          sI2CRegisters[sActiveReg++] = data;
        }
      }
    } else {
      I2C_SERCOM->I2CS.DATA.reg = sI2CRegisters[sActiveReg++];
    }
  }

  // Ensure we don't overflow our registers.
  if (sActiveReg >= I2C_NUM_REGISTERS) {
    sActiveReg = 0;
  }

  if (I2C_SERCOM->I2CS.INTFLAG.bit.PREC) {
    // No action to take on a stop bit, so just clear the flag.
    I2C_SERCOM->I2CS.INTFLAG.reg = SERCOM_I2CS_INTFLAG_PREC;
  }
}

uint8_t i2c_register_get(I2cRegister_t addr) {
  NVIC_DisableIRQ(I2C_IRQN);
  uint8_t ret = sI2CRegisters[addr];
  NVIC_EnableIRQ(I2C_IRQN);
  return ret;
}

void i2c_register_set(I2cRegister_t addr, uint8_t val) {
  if (addr >= I2C_NUM_REGISTERS) {
    return;
  }
  if (sI2CRegisters[addr] == val) {
    return;
  }

  NVIC_DisableIRQ(I2C_IRQN);
  sI2CRegisters[addr] = val;
  gpio_set_pin_level(PIN_INT, 0);
  NVIC_EnableIRQ(I2C_IRQN);
}

void i2c_suspend(bool suspend) {
  I2C_SERCOM->I2CS.CTRLA.bit.ENABLE = !suspend;
  while (I2C_SERCOM->I2CS.SYNCBUSY.bit.ENABLE)
    ;

  if (suspend) {
    gpio_set_pin_function(PIN_SCL, GPIO_PIN_FUNCTION_OFF);
  } else {
    gpio_set_pin_function(PIN_SCL, I2C_SCL_PINMUX);
  }
}
