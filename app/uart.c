/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "uart.h"

#include <atmel_start.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "component/gclk.h"
#include "component/sercom.h"
#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "pio/samd21e18a.h"
#include "samd21e18a.h"

#ifdef LEGACY_PINOUT

#define UART_SERCOM SERCOM2
#define UART_IRQ_HANDLER SERCOM2_Handler
#define UART_IRQN SERCOM2_IRQn
// RX on pad 3, TX on pad 2 (note: this value is not an index! see table 25-9)
#define UART_CTRLA_PADS \
  (SERCOM_USART_CTRLA_RXPO(3) | SERCOM_USART_CTRLA_TXPO(1))
#define UART_APBCMASK PM_APBCMASK_SERCOM2
#define UART_CLKID GCLK_CLKCTRL_ID_SERCOM2_CORE

#else

#define UART_SERCOM SERCOM0
#define UART_IRQ_HANDLER SERCOM0_Handler
#define UART_IRQN SERCOM0_IRQn
// RX on pad 1, TX on pad 0
#define UART_CTRLA_PADS \
  (SERCOM_USART_CTRLA_RXPO(1) | SERCOM_USART_CTRLA_TXPO(0))
#define UART_APBCMASK PM_APBCMASK_SERCOM0
#define UART_CLKID GCLK_CLKCTRL_ID_SERCOM0_CORE

#endif

#define UART_RX_BUFFER_SIZE (256)

// Ringbuffer for holding on to received UART bytes between USB CDC writes.
static char sRxBuffer[UART_RX_BUFFER_SIZE];
static size_t sRxBufferReadPos;
static size_t sRxBufferWritePos;

void UART_IRQ_HANDLER() {
  while (UART_SERCOM->USART.INTFLAG.bit.RXC) {
    sRxBuffer[sRxBufferWritePos] = UART_SERCOM->USART.DATA.reg;
    sRxBufferWritePos = (sRxBufferWritePos + 1) % UART_RX_BUFFER_SIZE;
    if (sRxBufferWritePos == sRxBufferReadPos) {
      sRxBufferWritePos = (sRxBufferReadPos - 1) % UART_RX_BUFFER_SIZE;
    }
  }
}

static void uart_set_baud(uint32_t baud_rate) {
  // Equation from the datasheet, Table 24-2, page 436.
  // (multiply by 8, to calculate fractional piece)
  uint32_t baud_times_8 = (48000000 * 8) / (8 * baud_rate);

  UART_SERCOM->USART.BAUD.FRAC.FP = (baud_times_8 % 8);
  UART_SERCOM->USART.BAUD.FRAC.BAUD = (baud_times_8 / 8);
}

void uart_init() {
  sRxBufferReadPos = 0;
  sRxBufferWritePos = 0;

// Set up the pins we're using. Both the correct direction and mux must be
// set.
#ifdef LEGACY_PINOUT
  gpio_set_pin_direction(PIN_PA14, GPIO_DIRECTION_OUT);
  gpio_set_pin_function(PIN_PA14, PINMUX_PA14C_SERCOM2_PAD2);
  gpio_set_pin_direction(PIN_PA15, GPIO_DIRECTION_IN);
  gpio_set_pin_function(PIN_PA15, PINMUX_PA15C_SERCOM2_PAD3);
#else
  gpio_set_pin_direction(PIN_PA04, GPIO_DIRECTION_OUT);
  gpio_set_pin_function(PIN_PA04, PINMUX_PA04D_SERCOM0_PAD0);
  gpio_set_pin_direction(PIN_PA05, GPIO_DIRECTION_IN);
  gpio_set_pin_function(PIN_PA05, PINMUX_PA05D_SERCOM0_PAD1);
#endif

  // Connect GCLK0 to UART_SERCOM.
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | UART_CLKID;

  // Wait for the write to complete.
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Enable the bvs clock for UART_SERCOM.
  PM->APBCMASK.reg |= UART_APBCMASK;

  // Reset into a consistent (and disabled) state.
  UART_SERCOM->USART.CTRLA.bit.SWRST = 1;
  while (UART_SERCOM->USART.SYNCBUSY.bit.SWRST)
    ;

  UART_SERCOM->USART.CTRLA.reg =
      // Use the internal clock, which gives us a max baud rate of 4MHz.
      SERCOM_USART_CTRLA_MODE_USART_INT_CLK |
      // Fractional baud rate generation, with 8x oversampling.
      SERCOM_USART_CTRLA_SAMPR(3) |
      // Use little-endian data order.
      SERCOM_USART_CTRLA_DORD |
      // Use the correct pads for the pinout.
      UART_CTRLA_PADS;

  UART_SERCOM->USART.CTRLB.reg =
      SERCOM_USART_CTRLB_TXEN | SERCOM_USART_CTRLB_RXEN;
  while (UART_SERCOM->USART.SYNCBUSY.bit.CTRLB)
    ;

  uart_set_baud(115200);

  UART_SERCOM->USART.INTENSET.bit.RXC = 1;
  NVIC_EnableIRQ(UART_IRQN);

  UART_SERCOM->USART.CTRLA.bit.ENABLE = 1;
  while (UART_SERCOM->USART.SYNCBUSY.bit.ENABLE)
    ;
}

bool uart_getc(char* c) {
  if (sRxBufferReadPos == sRxBufferWritePos) {
    return false;
  }
  *c = sRxBuffer[sRxBufferReadPos];
  sRxBufferReadPos = (sRxBufferReadPos + 1) % UART_RX_BUFFER_SIZE;
  return true;
}

void uart_putc(char c) {
  while (!(UART_SERCOM->USART.INTFLAG.bit.DRE)) {
  }
  UART_SERCOM->USART.DATA.reg = c;
}

void uart_puts(char* s) {
  // TODO: implement via one big memcpy, rather than going one char at a time.
  while (*s != '\0') {
    uart_putc(*s);
    s++;
  }
}

int uart_printf(const char* format, ...) {
  char str[128];
  va_list args;
  va_start(args, format);
  int len = vsnprintf(str, 128, format, args);
  va_end(args);

  if (len > 0 && len <= 128) {
    uart_puts(str);
    return len;
  }
  return -1;
}

void uart_configure(uint32_t baud_rate) {
  // Max baud rate is reference clock rate divided by samples per bit. Ignore
  // any requests higher than this.
  if (baud_rate > 40000000 / 16) {
    return;
  }

  UART_SERCOM->USART.CTRLA.bit.ENABLE = 0;
  while (UART_SERCOM->USART.SYNCBUSY.bit.ENABLE)
    ;

  uart_set_baud(baud_rate);

  UART_SERCOM->USART.CTRLA.bit.ENABLE = 1;
  while (UART_SERCOM->USART.SYNCBUSY.bit.ENABLE)
    ;
}
