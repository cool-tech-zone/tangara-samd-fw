/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "spi.h"

#include <atmel_start.h>
#include <stdint.h>

#include "component/sercom.h"
#include "gpio.h"
#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "pio/samd21e18a.h"

#ifdef LEGACY_PINOUT

#define SPI_SERCOM SERCOM0
#define SPI_IRQ_HANDLER SERCOM0_Handler
#define SPI_IRQN SERCOM0_IRQn
#define SPI_APBCMASK PM_APBCMASK_SERCOM0
#define SPI_CLKID GCLK_CLKCTRL_ID_SERCOM0_CORE
// DI on pad 0, DO on pad 2, CLK on pad 3 (see table 26-7)
#define SPI_CTRLA_PADS (SERCOM_SPI_CTRLA_DIPO(0) | SERCOM_SPI_CTRLA_DOPO(1))

#else

#define SPI_SERCOM SERCOM1
#define SPI_IRQ_HANDLER SERCOM1_Handler
#define SPI_IRQN SERCOM1_IRQn
#define SPI_APBCMASK PM_APBCMASK_SERCOM1
#define SPI_CLKID GCLK_CLKCTRL_ID_SERCOM1_CORE
// DI on pad 2, DO on pad 0, CLK on pad 1 (see table 26-7)
#define SPI_CTRLA_PADS (SERCOM_SPI_CTRLA_DIPO(2) | SERCOM_SPI_CTRLA_DOPO(0))

#endif

void spi_init() {
  // Mux the pins we're using.
#ifdef LEGACY_PINOUT
  gpio_set_pin_function(PIN_POCI, PINMUX_PA08C_SERCOM0_PAD0);
  gpio_set_pin_function(PIN_PICO, PINMUX_PA10C_SERCOM0_PAD2);
  gpio_set_pin_function(PIN_SCLK, PINMUX_PA11C_SERCOM0_PAD3);
#else
  gpio_set_pin_function(PIN_POCI, PINMUX_PA18C_SERCOM1_PAD2);
  gpio_set_pin_function(PIN_PICO, PINMUX_PA00D_SERCOM1_PAD0);
  gpio_set_pin_function(PIN_SCLK, PINMUX_PA01D_SERCOM1_PAD1);
#endif

  // PIN_CS is used as an ordinary GPIO; hardware CS control isn't appropriate,
  // as we need to hold CS low longer than the hardware control wants to.
  gpio_set_pin_direction(PIN_CS, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_CS, GPIO_PULL_OFF);
  gpio_set_pin_level(PIN_CS, true);

  // Connect GCLK0 to SPI_SERCOM.
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | SPI_CLKID;

  // Wait for the write to complete.
  while (GCLK->STATUS.bit.SYNCBUSY)
    ;

  // Enable the bus clock for SPI_SERCOM.
  PM->APBCMASK.reg |= SPI_APBCMASK;

  // Reset into a consistent (and disabled) state.
  SPI_SERCOM->SPI.CTRLA.bit.SWRST = 1;
  while (SPI_SERCOM->SPI.SYNCBUSY.bit.SWRST)
    ;

  // For SD cards, we need CPOL=0, CPHA=0.
  SPI_SERCOM->SPI.CTRLA.reg =
      // We are controller to the SD card
      SERCOM_SPI_CTRLA_MODE_SPI_MASTER |
      // Use the right pads for the current pinout.
      SPI_CTRLA_PADS;

  // Enable receiving data.
  SPI_SERCOM->SPI.CTRLB.reg = SERCOM_SPI_CTRLB_RXEN;

  while (SPI_SERCOM->SPI.SYNCBUSY.bit.CTRLB)
    ;

  SPI_SERCOM->SPI.CTRLA.bit.ENABLE = 1;
  while (SPI_SERCOM->SPI.SYNCBUSY.bit.ENABLE)
    ;
}

void spi_set_baud(uint32_t hz) {
  // SPI_SERCOM must be disabled before making changes to the BAUD register.
  SPI_SERCOM->SPI.CTRLA.bit.ENABLE = 0;
  while (SPI_SERCOM->SPI.SYNCBUSY.bit.ENABLE)
    ;

  // See table 24-2.
  // BAUD = (24MHz / (2 * hz)) - 1
  // Highest possible baud rate is 24MHz, when BAUD=0
  // Lowest possible baud rate is a little under 100kHz, when BAUD=255
  SPI_SERCOM->SPI.BAUD.reg = (48000000 / (2 * hz)) - 1;

  SPI_SERCOM->SPI.CTRLA.bit.ENABLE = 1;
  while (SPI_SERCOM->SPI.SYNCBUSY.bit.ENABLE)
    ;
}

uint8_t spi_transmit(uint8_t byte) {
  // Wait for the register to be ready for writing.
  while (!(SPI_SERCOM->SPI.INTFLAG.bit.DRE))
    ;

  SPI_SERCOM->SPI.DATA.reg = byte;

  // Wait for the register to be ready for reading.
  while (!(SPI_SERCOM->SPI.INTFLAG.bit.RXC))
    ;

  return SPI_SERCOM->SPI.DATA.reg;
}

void spi_transmit_bulk(uint8_t* tx, uint8_t* rx, size_t len) {
  if (len == 0) {
    return;
  }
  size_t transmit_index = 0;
  size_t receive_index = 0;
  if (rx) {
    while (transmit_index < len) {
      // Send a couple of bytes to preload the double-buffered RX register.
      while (transmit_index < 2) {
        while (!SPI_SERCOM->SPI.INTFLAG.bit.DRE)
          ;
        SPI_SERCOM->SPI.DATA.reg = tx ? tx[transmit_index] : 0xFF;
        transmit_index++;
      }
      // Send+Receive the rest of the data.
      while (transmit_index < len) {
        while (!SPI_SERCOM->SPI.INTFLAG.bit.RXC)
          ;
        rx[receive_index++] = SPI_SERCOM->SPI.DATA.reg;
        SPI_SERCOM->SPI.DATA.reg = tx ? tx[transmit_index] : 0xFF;
        transmit_index++;
      }
      // Drain the RX buffers.
      while (receive_index < len) {
        while (!SPI_SERCOM->SPI.INTFLAG.bit.RXC)
          ;
        rx[receive_index++] = SPI_SERCOM->SPI.DATA.reg;
      }
    }
  } else if (tx) {
    // Don't waste time receiving bytes if we don't care about them.
    SPI_SERCOM->SPI.CTRLB.bit.RXEN = 0;
    while (SPI_SERCOM->SPI.SYNCBUSY.bit.CTRLB)
      ;

    // Shift out the entire transmit buffer.
    while (transmit_index < len) {
      while (!SPI_SERCOM->SPI.INTFLAG.bit.DRE)
        ;
      SPI_SERCOM->SPI.DATA.reg = tx[transmit_index++];
    }

    // Wait for the transmission to finish.
    while (!(SPI_SERCOM->SPI.INTFLAG.bit.TXC))
      ;

    SPI_SERCOM->SPI.CTRLB.bit.RXEN = 1;
    while (SPI_SERCOM->SPI.SYNCBUSY.bit.CTRLB)
      ;
  }
}
