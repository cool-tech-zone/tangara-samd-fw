/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "gpio.h"

void i2c_init(void);
void i2c_suspend(bool);

typedef enum {
  // Read-only registers
  FIRMWARE_MAJOR = 0,
  FIRMWARE_MINOR = 1,
  CHARGE_STATUS = 2,
  USB_STATUS = 3,
  // RW registers
  POWER_CONTROL = 4,
  USB_CONTROL = 5,
} I2cRegister_t;

uint8_t i2c_register_get(I2cRegister_t addr);
void i2c_register_set(I2cRegister_t addr, uint8_t val);
