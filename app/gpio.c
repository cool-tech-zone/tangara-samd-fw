/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "gpio.h"

#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "i2c.h"
#include "pio/samd21e18a.h"

void gpio_early_init() {
  gpio_set_pin_direction(PIN_ESP_EN, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_ESP_EN, GPIO_PULL_UP);
  gpio_set_pin_level(PIN_ESP_EN, false);
}

void gpio_init() {
  // Do the other half of the ESP32-related pins first.
  gpio_set_pin_direction(PIN_ESP_BOOT, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_ESP_BOOT, GPIO_PULL_OFF);
  gpio_set_pin_level(PIN_ESP_BOOT, true);

  // BMS-related pins
  gpio_set_pin_direction(PIN_CHG_STAT1, GPIO_DIRECTION_IN);
  gpio_set_pin_pull_mode(PIN_CHG_STAT1, GPIO_PULL_UP);
  gpio_set_pin_direction(PIN_CHG_STAT2, GPIO_DIRECTION_IN);
  gpio_set_pin_pull_mode(PIN_CHG_STAT2, GPIO_PULL_UP);
  gpio_set_pin_direction(PIN_CHG_POWER_OK, GPIO_DIRECTION_IN);
  gpio_set_pin_pull_mode(PIN_CHG_POWER_OK, GPIO_PULL_OFF);

  gpio_set_pin_direction(PIN_CHG_SEL, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_CHG_SEL, GPIO_PULL_UP);
  gpio_set_pin_level(PIN_CHG_SEL, false);

#ifndef LEGACY_PINOUT
  gpio_set_pin_direction(PIN_CHG_PROG, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_CHG_PROG, GPIO_PULL_UP);
  gpio_set_pin_level(PIN_CHG_PROG, false);
#endif

  // USB-C Interface
  gpio_set_pin_direction(PIN_USB_TCC0, GPIO_DIRECTION_IN);
  gpio_set_pin_pull_mode(PIN_USB_TCC0, GPIO_PULL_OFF);
  gpio_set_pin_direction(PIN_USB_TCC1, GPIO_DIRECTION_IN);
  gpio_set_pin_pull_mode(PIN_USB_TCC1, GPIO_PULL_OFF);

  // I2C Bus
  gpio_set_pin_direction(PIN_SDA, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_SDA, GPIO_PULL_OFF);
  gpio_set_pin_level(PIN_SDA, true);
  gpio_set_pin_direction(PIN_SCL, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_SCL, GPIO_PULL_OFF);
  gpio_set_pin_level(PIN_SCL, true);
  gpio_set_pin_direction(PIN_INT, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_INT, GPIO_PULL_OFF);
  gpio_set_pin_level(PIN_INT, true);

  // Board features
  gpio_set_pin_direction(PIN_SYS_PWR_EN, GPIO_DIRECTION_OUT);
  gpio_set_pin_pull_mode(PIN_SYS_PWR_EN, GPIO_PULL_UP);
  gpio_set_pin_level(PIN_SYS_PWR_EN, true);
}

// Values are based on TCC1 being the MSB and TCC0 being the LSB. See
// datasheet for BD91N01NUX, pp.4.
typedef enum UsbState {
  USB_DETACHED = 0b00,
  USB_500MA = 0b01,
  USB_1500MA = 0b10,
  USB_3000MA = 0b11,
} UsbState_t;

UsbState_t gpio_usb_state() {
  bool tcc1 = gpio_get_pin_level(PIN_USB_TCC1);
  bool tcc0 = gpio_get_pin_level(PIN_USB_TCC0);
  return (tcc1 << 1) | tcc0;
}

// Values are extracted from the datasheet for MCP73871, table 5-1 pp.25. Values
// don't correspond to anything.
typedef enum ChargeState {
  CHG_NO_BATTERY = 0b000,
  CHG_IN_PROGRESS = 0b001,
  CHG_COMPLETE = 0b010,
  CHG_FAULT = 0b011,
  CHG_LOW_BATTERY = 0b100,
  CHG_NO_EXT_POWER = 0b101,
  // 0b110 is unused / reserved.
  CHG_UNKNOWN = 0b111,
} ChargeState_t;

ChargeState_t gpio_charge_state() {
  bool stat1 = gpio_get_pin_level(PIN_CHG_STAT1);
  bool stat2 = gpio_get_pin_level(PIN_CHG_STAT2);
  // Active-low; we don't invert it to better match the table from the
  // datasheet, so rename it as the next best thing.
  bool no_ext_power = gpio_get_pin_level(PIN_CHG_POWER_OK);

  if (stat1 && stat2 && !no_ext_power) {
    return CHG_NO_BATTERY;
  }
  if (!stat1 && stat2 && !no_ext_power) {
    return CHG_IN_PROGRESS;
  }
  if (stat1 && !stat2 && !no_ext_power) {
    return CHG_COMPLETE;
  }
  if (!stat1 && !stat2 && !no_ext_power) {
    return CHG_FAULT;
  }
  if (!stat1 && stat2 && no_ext_power) {
    return CHG_LOW_BATTERY;
  }
  if (no_ext_power) {
    return CHG_NO_EXT_POWER;
  }
  return CHG_UNKNOWN;
}

static inline void set_chg_prog(bool val) {
#ifndef LEGACY_PINOUT
  gpio_set_pin_level(PIN_CHG_PROG, val);
#endif
}

void gpio_update_max_current(UsbState_t usb_state) {
  switch (usb_state) {
    case USB_1500MA:
    case USB_3000MA:
      // Set the 'USB' current limit to 500mA
      set_chg_prog(1);
      // Use the 'AC adapter' current limit if we're allowed it. Otherwise,
      // use the 'USB' current limit.
      gpio_set_pin_level(PIN_CHG_SEL, i2c_register_get(POWER_CONTROL) & 0b10);
      break;
    case USB_500MA:
      // Set the 'USB' current limit to 500mA
      set_chg_prog(1);
      // Use the 'USB' current limit.
      gpio_set_pin_level(PIN_CHG_SEL, 0);
      break;
    default:
    case USB_DETACHED:
      // Set the 'USB' current limit to 100mA
      set_chg_prog(0);
      // Use the 'USB' current limit.
      gpio_set_pin_level(PIN_CHG_SEL, 0);
      break;
  }
}

void gpio_task() {
  UsbState_t usb_state = gpio_usb_state();
  ChargeState_t charge_state = gpio_charge_state();

  gpio_update_max_current(usb_state);

  i2c_register_set(CHARGE_STATUS, usb_state | (charge_state << 2));
}
