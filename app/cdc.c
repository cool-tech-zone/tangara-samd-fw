/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include "cdc.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <tusb.h>

#include "class/cdc/cdc_device.h"
#include "device/usbd.h"
#include "hal_gpio.h"
#include "hpl_gpio.h"
#include "pio/samd21e18a.h"

#include "gpio.h"
#include "i2c.h"
#include "tick.h"
#include "uart.h"

void cdc_putc(char c) {
  if (!tud_ready()) {
    return;
  }
  tud_cdc_write_char(c);
}

void cdc_puts(char* c, size_t len) {
  if (!tud_ready()) {
    return;
  }
  tud_cdc_write(c, len);
}

int cdc_printf(const char* format, ...) {
  char str[128];
  va_list args;
  va_start(args, format);
  int len = vsnprintf(str, 128, format, args);
  va_end(args);

  if (len > 0 && len <= 128) {
    cdc_puts(str, len);
    return len;
  }
  return -1;
}

void cdc_flush() {
  tud_cdc_write_flush();
}

// Invoked when line state DTR & RTS are changed via SET_CONTROL_LINE_STATE
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts) {
  (void)itf;  // Unused.
  if (dtr && rts) {
    // Some terminal emulators set both of these at once during the initial
    // connection. Ignore this case, since we don't want the ESP to mysteriously
    // begin bootlooping when you plug it in.
    return;
  }
  gpio_set_pin_level(PIN_ESP_BOOT, !dtr);
  gpio_set_pin_level(PIN_ESP_EN, !rts);

  // Some consoles / flashers pull RTS only very briefly. Wait a moment for the
  // EN pin to actually fall before returning to ensure that pulling RTS to
  // reset works consistently.
  if (rts) {
    uint32_t drained_at = tick_millis();
    uint32_t now;
    do {
      now = tick_millis();
    } while (drained_at <= now && now <= drained_at + 100);
  }

  // If we don't suspend I2C before pulling dtr, then the I2C peripheral's
  // control will override ours.
  i2c_suspend(dtr);
  gpio_set_pin_level(PIN_SCL, !dtr);
}

// Invoked when line coding is change via SET_LINE_CODING
void tud_cdc_line_coding_cb(uint8_t itf,
                            cdc_line_coding_t const* p_line_coding) {
  (void)itf;  // Unused.
  // TODO: do we care about passing through parity bit changes etc as well?
  uart_configure(p_line_coding->bit_rate);
}
