/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

void spi_init(void);

void spi_set_baud(uint32_t hz);

uint8_t spi_transmit(uint8_t);
void spi_transmit_bulk(uint8_t* tx, uint8_t* rx, size_t len);
