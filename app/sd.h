/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

void sd_init(void);
void sd_task(void);
bool sd_is_ready(void);
uint32_t sd_block_count(void);
uint32_t sd_read(uint32_t block, uint8_t* buffer, uint32_t bufsize);
uint32_t sd_write(uint32_t block, uint8_t* buffer, uint32_t bufsize);
