/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <stddef.h>

extern int _heap_start;

__attribute__((used)) void* _sbrk(int incr) {
  static unsigned char* heap = NULL;
  unsigned char* prev_heap;

  if (heap == NULL) {
    heap = (unsigned char*)&_heap_start;
  }

  prev_heap = heap;
  heap += incr;

  return prev_heap;
}
