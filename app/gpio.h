/*
 * Copyright 2023 jacqueline <me@jacqueline.id.au>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <stdint.h>

#ifdef LEGACY_PINOUT

#define PIN_ESP_EN PIN_PA04
#define PIN_ESP_BOOT PIN_PA05

#define PIN_CHG_STAT1 PIN_PA00
#define PIN_CHG_STAT2 PIN_PA01
#define PIN_CHG_POWER_OK PIN_PA14
#define PIN_CHG_SEL PIN_PA19

#define PIN_USB_TCC0 PIN_PA02
#define PIN_USB_TCC1 PIN_PA03

#define PIN_SD_VDD_EN PIN_PA06
#define PIN_KEY_LOCK PIN_PA07
#define PIN_SYS_PWR_EN PIN_PA23

#define PIN_SDA PIN_PA16
#define PIN_SCL PIN_PA17
#define PIN_INT PIN_PA18

#define PIN_POCI PIN_PA08
#define PIN_PICO PIN_PA10
#define PIN_SCLK PIN_PA11
#define PIN_CS PIN_PA09

#else

#define PIN_ESP_EN PIN_PA14
#define PIN_ESP_BOOT PIN_PA09

#define PIN_CHG_STAT1 PIN_PA27
#define PIN_CHG_STAT2 PIN_PA28
#define PIN_CHG_POWER_OK PIN_PA23
#define PIN_CHG_SEL PIN_PA22
#define PIN_CHG_PROG PIN_PA19

#define PIN_USB_TCC0 PIN_PA07
#define PIN_USB_TCC1 PIN_PA06

#define PIN_SYS_PWR_EN PIN_PA03

#define PIN_SDA PIN_PA16
#define PIN_SCL PIN_PA17
#define PIN_INT PIN_PA15

#define PIN_POCI PIN_PA18
#define PIN_PICO PIN_PA00
#define PIN_SCLK PIN_PA01
#define PIN_CS PIN_PA02

#endif

void gpio_early_init();
void gpio_init();
void gpio_task();
